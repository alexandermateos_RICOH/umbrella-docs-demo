# Ejemplo de uso de Asciidoc para documentación técnica

## Generación de documentación en formato PDF

Desde el directorio raiz del proyecto ejecutar:

```shell
$ docker run --rm -v $(pwd):/documents/ asciidoctor/docker-asciidoctor asciidoctor-pdf -r asciidoctor-diagram --failure-level WARN -t docs/index.adoc

Input file: index.adoc
  Time to read and parse source: 0.00109
  Time to convert document: 0.20477
  Total time (read, parse and convert): 0.20587
```
